# S3 Browser

S3 browser (formally known as LLP-L) is a flask based application which builds a directory listing.

# Configuration

After filling in config.py run the following for a local dev env:

```shell
virtualenv -p python3 venv --always-copy
source venv/bin/activate
export FLASK_APP=app.py
````

# Lambda/Zappa

This application is intended to be ran and deployed with Zappa/Lambda.

To setup zappa run:

```shell
zappa init
zappa deploy dev
```

