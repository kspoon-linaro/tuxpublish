from flask import Flask, render_template, request, redirect, url_for, flash, \
    Response, session
from flask_bootstrap import Bootstrap
from filters import datetimeformat, file_type, file_name, file_size, folder_name
from resources import gen_s3_folders, gen_s3_files, create_signed_url

app = Flask(__name__)
Bootstrap(app)
app.secret_key = 'secret'
app.jinja_env.filters['datetimeformat'] = datetimeformat
app.jinja_env.filters['file_type'] = file_type
app.jinja_env.filters['file_name'] = file_name
app.jinja_env.filters['file_size'] = file_size
app.jinja_env.filters['folder_name'] = folder_name
import os


@app.route('/<path:dummy>')
def fallback(dummy):
    url = dummy
    if not url.endswith('/'):
        url += '/'
    file = []
    folder = []

    gen_folders = gen_s3_folders(prefix=url)

    for fol in gen_folders:
        if url in fol:
            folder.append(fol)

    for fil in gen_s3_files(prefix=url):
        if fil['Size'] != 0:
            file.append(fil)
            if fil['Key'] == url:
                return create_signed_url(url)

    if file or folder:
            return render_template('files.html', files=file, folders=folder)
    else:
        return render_template('404.html'), 404

@app.route('/')
def index():
    return render_template("index.html")


if __name__ == "__main__":
    app.run()
